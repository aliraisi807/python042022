from email import header
from genericpath import sameopenfile
from itertools import islice
# from msilib import sequence
from optparse import Values
from pickle import TRUE
from re import A, S
from tkinter import Y, Button
from typing import Counter
from typing_extensions import Self
from xml.dom.minidom import Element
from numpy import number
# import pandas as pd
from pyparsing import line
from torch import flip, outer, true_divide

# """/home/sigma/anaconda3/envs/scispacy/bin/python /home/sigma/nanobubbles/script/tst/tst_main.py

# """
# words = "s the ability of a computer or a robot controlled by a computer to do tasks that are usually done by humans because they require human intelligence and discernment".split()
# def is_prime(x):
#     for i in range(2, int(x**0.5)+1):
#         if x%i ==0:
#             return False
#     return TRUE

# def lucas():
#     yield 2
#     a = 2
#     b = 1

#     while True:
#         yield b
#         a, b = b, a + b
    

#  iteration tools
# # any objects that follow iterable protocols like enumerate() and sum()
# # itertools.islice(): perform lazy slicing of any iterator
# # to get fist 1000 prime we need to do from all_primes just take
# # first 1000 primes
# # then how to generate primes? previously we used range() to make a raw test
# # but range must be finite which bounded on both end
# # what should we do for having an open ended range that is possible with
# # iteratools.count()
# from itertools import count, islice
# thousand_primes = islice((x for x in count() if is_prime(x)), 1000)
# # print(thousand_primes)
# print(list(thousand_primes)[-10:])
# # now sum of the 

# print(sum(islice((x for x in count() if is_prime(x)), 1000)))

# # two other very useful builtin any() and all()
# # any takes elements and tell if any of the element is true ([])
# # all() takes elements and tell if all the elements are true
# # take any in specific range if there is prime
# print(any(is_prime(x) for x in range(1328, 1361)))

# print(all(name == name.title() for name in ['London', 'Paris','Tokyo','New York','Sydney','Kuala Lumpur']))

# # title()
# # the title() method returns a string where the first character in every word is upper case. like a heade
# # 
# txt = "Welcome to my world"
# x = txt.title()
# print(x)

# # zip(): synchronize iteration across two or more iterables.
# # example column of temprature for sunday and monday

# sunday = [12, 14, 15, 15, 17, 21, 22, 22, 23, 22, 20, 18]
# monday = [13, 14, 14, 14, 16, 20, 21, 22, 22, 21, 19, 17]

# # for item in zip(sunday, monday):
# #     print(item)

# # we can see from result that zip take tuples when iterated.
# # this means that we can use for tuple packing in the for loop
# for sun, mon in zip(sunday, monday):
#     print("average =", (sun + mon)/2)


# # lets add tuesday
# tuesday = [2, 2, 3, 7, 9, 10, 11, 12, 10, 9, 8, 8]
# for temps in zip(sunday, monday, tuesday):
#     # print(f"min = {min(temps):4.1f}, max={max(temps):4.1f}, average={sum(temps)/len(temps):4.1f}")
#     print(f"min={min(temps):4.1f}, max={max(temps):4.1f}, average={sum(temps)/len(temps):4.1f}")

# # 
# from itertools import chain

# temperatures = chain(sunday, monday, tuesday)
# print(all( t>0 for t in temperatures))

# # put all piece togather and leave computer to comput lucas prime
# for x in (p for p in lucas() if is_prime(p)):
#     print(x)

# # all object in python has type
# # print(type(5))
# # print(type("python"))
# # print(type([1,2,3]))
# # print(type(x*x for x in [2, 4, 6]))

# # define class first how to define class and we use pass
# # means do nothing then we assing the class to variable to 
# # ... check which type it is
# class Flight:
#     pass

# f = Flight
# print(type(f))
# # lets make class more interesitn by instance method
# # instance method returns the flight number
# # methods are functions that could fine in the class
# # but instance method is object used for creating object
# # ==================================================================
# # second class declaration with default instances
# class Flight:

#     def number(self):
        
#         return "SN060"

# # # construct Flight instance and bound to name f then 
# # # ... call number on f
# f = Flight()
# print(f.number()) 
# # we did not directly called number on f which this mean
# # literaly this is the syntax Flight.number(f)


# # in above case the class is not we need to make Flight configurable at
# # at the point that flight is created
# # to do that we need to write an initialization method
# # the danderned init is initializer, not a constructor.
# # self is similar to this in java, c#, or c++.
# # in python constructor is done by run time system it check for 
# # existing of instance initializer called in present

# # 3 ================================================================
# # third how to initialize instance
# class Flight:
#     def __init__(self, number):
# # __D__ create a new reference and point to argument "number"
# # __ configure an object which already exist
# #  the initializer does not return anything it just modify an object that return to itself

# class Flight:

#     def __init__(self, number):
# #         # use _number because it avoid name class with number()
#         self._number = number

#     def number(self):
# #             # modify return to access _number attribute and return it
#         return self._number

# f = Flight("SN060")
# print(f.number())

# # 4 ============================================================
# # in python everything is public
# # how to create class invarient in python

# class Flight:
#     def __init__(self, number):
#         if not number[:2].isalpha():
#             raise ValueError(f"no airline code in '{number}'")
#         if not number[:2].isupper():
#             raise ValueError(f"invalid airline code '{number}'")
#         if not (number[2:].isdigit() and int(number[2:]) <= 9999):
#             raise ValueError(f"invalid route number '{number}'")

#         self._number = number
#     def number(self):
#         return self._number
    
#     def airline(self):
#         return self.number[:2]

# f = Flight("Ssn")
# print(f.number())

# # 5 w3school simple explanation of how to create calss and object
# # class MyClass:
# #     # lets create property for class
# #     x = 5
# # # now create object and print the value of x
# # p1 = MyClass()
# # print(p1.x)

# # class person
# class Person:
#     def __init__(self, name, age):
#         self._name = name
#         self._age = age
# # __init__() is called automatically every time the class is being used to create new object
# p1 = Person("john", 36)
# print(p1._name)
# print(p1._age)
# p2 = Person("ali", 28)
# print(p2._name)
# print(p2._age)

# ======== create method in the Person class: =============

# class Person:
#     def __init__(self, name, age):
#         self._name = name
#         self._age = age
    
#     def myfunc(self):
#         print("hello my name is" + self._name)
# p1 = Person("john", 36)
# print(p1.myfunc())

# ============self parameter of the method==================
# class Person:
#     def __init__(mysillyobjet, name, age):
#         mysillyobjet._name = name
#         mysillyobjet._age = age
#     def myfunc(abc):
#         print("hello my name is "+ abc._name)
# p1 = Person("john", 36)
# p1.myfunc()

# ====== how to modify object properties======

# class Person:
#     def __init__(mysillyobjet, name, age):
#         mysillyobjet._name = name
#         mysillyobjet._age = age
#     def myfunc(abc):
#         print("hello my name is "+ abc._name)
# p1 = Person("john", 36)
# print(p1._age)
# p1._age = 40
# print(p1._age)

# ======== how to delete object property==============
# class Person:
#     def __init__(mysillyobjet, name, age):
#         mysillyobjet._name = name
#         mysillyobjet._age = age
#     def myfunc(abc):
#         print("hello my name is "+ abc._name)
# p1 = Person("john", 36)
# del p1
# print(p1._name)

# ======================second class =================================

# class Flight:

#     def __init__(self, number):
#         if not number[:2].isalpha():
#             raise ValueError(f"no airline code in '{number}'")
#         if not number[:2].isupper():
#             raise ValueError(f"invalid airline code '{number}'")
#         if not (number[2:].isdigit() and int(number[2:]) <= 9999):
#             raise ValueError(f"invalid route number '{number}'")
#         self._number = number
# # now construct flight using flight number
# f = Flight("SN060")
# will not produce any logical error because it based on our set condition
# f= Flight("1N060")
# produced error because the first to element is not letter
# similarly for the remainig conditions

# now are sure that flight number produce the correct output we add
# the second method
# ============= add the second method==================

# class Flight:

#     def __init__(self, number):
#         if not number[:2].isalpha():
#             raise ValueError(f"no airline code in '{number}'")
#         if not number[:2].isupper():
#             raise ValueError(f"invalid airline code '{number}'")
#         if not (number[2:].isdigit() and int(number[2:]) <= 9999):
#             raise ValueError(f"invalid route number '{number}'")
#         self._number = number

#     def number(self):
#         return self._number

#     def airline(self):
#         return self._number[:2]
# # now construct flight using flight number
# # class for accept seat bookings
# class Aircraft:
#     def __init__(self, registration, model, num_rows, num_seats_per_row):
#         self._registration = registration
#         self._model = model
#         self._num_rows = num_rows
#         self._num_seats_per_row = num_seats_per_row
   
#     def registration(self):
#             return self._registration

#     def model(self):
#             return self._model

#     def seating_plan(self):
#             # 
#             return(range(1, self._num_rows + 1), "ABCSEFGHJK"[:self._num_seats_per_row])
# # now lets construct the airline 
# # a = Aircraft("G-EUPT", "Airbus A319", num_rows=22, num_seats_per_row=6)

# print(a.registration())

# print(a.model())

# print(a.seating_plan())

# ================collaborating classes======================================
# # we modify our Flight class to accept aircraft object when it deconstructed
# class Flight:
#     """a flight with a particular passsenger aircraft."""
#     def __init__(self, number, aircraft):

#         if not number[:2].isalpha():
#             raise ValueError(f"no airline code in '{number}'")
#         if not number[:2].isupper():
#             raise ValueError(f"invalid airline code '{number}'")
#         if not (number[2:].isdigit() and int(number[2:]) <= 9999):
#             raise ValueError(f"invalid route number '{number}'")
#         self._number = number
#         self._aircraft = aircraft
#     # now we follow the law of the meter by

#     def aircraft_model(self):
#         return self._aircraft.model()

#     def number(self):
#         return self._number

#     def airline(self):
#         return self._number[:2]

# f = Flight("BA758", Aircraft("G-EUPT", "Airbus A319", num_rows=22, num_seats_per_row=6))

# print(f.aircraft_model()) 

# # from above code we understand how to access directly to attribute of class with another class
# # without 
# ================seat booking data structure=======================================

# class for accept seat bookings
# class Aircraft:
#     def __init__(self, registration, model, num_rows, num_seats_per_row):
#         self._registration = registration
#         self._model = model
#         self._num_rows = num_rows
#         self._num_seats_per_row = num_seats_per_row
   
#     def registration(self):
#             return self._registration

#     def model(self):
#             return self._model

#     def seating_plan(self):
#             # 
#             return(range(1, self._num_rows + 1), "ABCSEFGHJK"[:self._num_seats_per_row])
# now lets construct the airline 
# a = Aircraft("G-EUPT", "Airbus A319", num_rows=22, num_seats_per_row=6)

# print(a.registration())

# print(a.model())

# print(a.seating_plan())


# ===========================Flight class ========================================

# print(f.aircraft_model()) 
# leading _ remind us what is public and what is not
# print(f._seating)
# that is accurate but not beautiful lets try with prety print
# from pprint import pprint as pp
# pp(f._seating)
# now we add behavior to flight to keep alocate seats to passengers

# class Flight:
#     """a flight with a particular passsenger aircraft."""
#     # with __init__ start construction of object
#     def __init__(self, number, aircraft):
#         # with below conditions we check the two first character is alphabet

#         if not number[:2].isalpha():
#             raise ValueError(f"no airline code in '{number}'")
#         # is those two fist alphabet is uppercase?
#         if not number[:2].isupper():
#             raise ValueError(f"invalid airline code '{number}'")
#         # check if after those two uppercase alphabet the number is < 999
#         if not (number[2:].isdigit() and int(number[2:]) <= 9999):
#             raise ValueError(f"invalid route number '{number}'")
#         # construct the object of two parameters
#         self._number = number
#         self._aircraft = aircraft
        
#         # by aircraft object we access to the method of Aircraft class to check 
#         # for other details like seats and rows
#         rows, seats = self._aircraft.seating_plan()
#         # in a dictionary for each key letter print value None then store in a list
#         # _ in rows means that i dont need iterator variable inside the loop for any operation
#         self._seating = [None] + [{letter: None for letter in seats} for _ in rows ]

#     # now we allocate seats to passenger by adding funciton
#     def allocate_seat(self,seat, passenger):

#         """allocate a seat to a passenger.
#         Args:
#             seat: a seat designator such as '12C' or '21F'.
#         Raises:
#             valueError: if the seat is unavailable
#         """
#         # for allocating 'seat' we need to data like row number and and row letter
#         # for this information we need to _parse_seat function 
#         # the returned row na letter from _parse_seat()
#         row, letter = self._parse_seat(seat)
#         # check if row number and letter is not None then it has occupied 
#         if self._seating[row][letter] is not None:
#             raise ValueError(f"seat {seat} already occcupied")
#         # otherwise set that combination of row number and letter to passenger
#         self._seating[row][letter] = passenger
        
#     # checking the details of seat within function _parse_seat
#     def _parse_seat(self, seat):
#         # check the rows and seat letters must match with the format of function seating_plan
#         rows, seat_letters = self._aircraft.seating_plan()
#         # the letter must be one letter from end side
#         letter = seat[-1]
#         # and the letter must be from seat letter specified in seating_plan function
#         if letter not in seat_letters:
#             raise ValueError(f"invalid seat letter {letter}")
#         # checking for row number before the last letter
#         row_text = seat[:-1]
#         try:
#             # if it is integer then store in row otherwise give valueError
#             row = int(row_text)
#         except ValueError:
#             raise ValueError(f"invalid seat row {row_text}")
#         # check if the row number is in the range of row numbers of seating_plan()
#         if row not in rows:
#             raise ValueError(f"invalid row number {row}")
#         # if the row number and letter is according to seating_plan() then return it
#         return row, letter
#     # if change the seat of passenger
#     def relocate_passenger(self, from_seat, to_seat):
#         """relocate a passenger to a different seat."""
#         # check if the the seat we want to relocate with another seat if the from_seat of
#         # .. them are not empty then relocate with the to_seat which is not occupied.
#         from_row, from_letter = self._parse_seat(from_seat)
#         if self._seating[from_row][from_letter] is None:
#             raise ValueError(f"no passenger to relocate in seat {from_seat}")

#         to_row, to_letter = self._parse_seat(to_seat)
#         if self._seating[to_row][to_letter] is not None:
#             raise ValueError(f"seat {to_seat} already occupied")
    
 
#         self._seating[to_row][to_letter] = self._seating[from_row][from_letter]
#         # after relocate the from_seat must set to empty in order to be available
#         self._seating[from_row][from_letter] = None
#     # check how many seats are still available
#     def num_available_seats(self):
#         # return the total of available seats by checking valuse of row
#         # return sum(sum( 1 for s in row.values() if s is None) for row in self._seating if row is not None)
#         # return sum of rows which is None 
#         return sum(sum( 1 for s in row.values() if s is None) for row in self._seating if row is not None)

#     # making boarding card
#     def make_boarding_cards(self, card_printer):
#         # for each passenger and seat in sorted _passenger_seat()
#         for passenger, seat in sorted(self._passenger_seats()):
#             # print card with passenger, seat, number of flight number and aircraft model
#             card_printer(passenger, seat, self.number(), self.aircraft_model())

#     # generator for passenger seat details
#     def _passenger_seats(self):
#         """an iterable series of passenger seating location."""
#         # take row number and seat letter from seating_plan()
#         row_numbers, seat_letters = self._aircraft.seating_plan()
#         for row in row_numbers:
#             for letter in seat_letters:
#                 # putting row number + letter store in passenger
#                 passenger = self._seating[row][letter]
#                 # check if passenger exist then allocate row + letter fot he/she
#                 if passenger is not None:
#                     # if the passenger is not None yield the passenger and seats information 
#                     yield (passenger, f"{row}{letter}")

 
#     # now we follow the law of the meter by

#     def aircraft_model(self):
#         return self._aircraft.model()

#     def number(self):
#         return self._number

#     def airline(self):
#         return self._number[:2]


# # ============= class aircraft===========================================
# # this class gives detail about registraion, model num of rows ans and number of
# # .. num of seats for each row
# # class Aircraft:
# #     def __init__(self, registration, model, num_rows, num_seats_per_row):
# #         # now construct object of each parameter
# #         self._registration = registration
# #         self._model = model
# #         self._num_rows = num_rows
# #         self._num_seats_per_row = num_seats_per_row
# #     # return registration but have question in here?
# #     def registration(self):
# #             return self._registration
# #     # return model number
# #     def model(self):
# #             return self._model
# #     # this function generating row numbers + letter
# #     def seating_plan(self):
# #             # 
# #             return(range(1, self._num_rows + 1), "ABCSEFGHJK"[:self._num_seats_per_row])
#     # this function printing the details like 
# def console_card_printer(passenger, seat, flight_number, aircraft):
#     output = f"| Name: {passenger}" \
#                 f"  Flight: {flight_number}" \
#                 f"  Seat: {seat}"  \
#                 f"  Aircraft: {aircraft}"  \
#                 " |"
#     banner = "+" + "-" * (len(output) - 2) + "+"
#     border = "|" + " " * (len(output) - 2) + "|"
#     lines = [banner, border, output, border, banner]
#     card = "\n".join(lines)
#     print(card)
#     print()

# this function based on constructed object of class Flight alocate seate and return object

# def make_flight():
#     # with this function we create flight object
#     f = Flight("BA758", Aircraft("G-EUPT", "Airbus A319", num_rows=22, num_seats_per_row=6))
#     # by flight object accessed to allocate_seat function to allocate seats
#     f.allocate_seat("12A", "Guido van Rossum")
#     f.allocate_seat("15F", "Bjarne stroustrup")
#     f.allocate_seat("15E", "Anders Hejlsberg")
#     f.allocate_seat("13C", "john mccarthy")
#     f.allocate_seat("14S", "Rich Hickey")
#     return f
# # create new object of the funciton to access make_boarding_cards() and print boarding pass
# f = make_flight()
# f.make_boarding_cards(console_card_printer)
# ====================================polymorphism==============================================
# class Aircraft:
#     def __init__(self, registration, model, num_rows, num_seats_per_row):
#         # now construct object of each parameter
#         self._registration = registration
#         self._model = model
#         self._num_rows = num_rows
#         self._num_seats_per_row = num_seats_per_row
#     # return registration but have question in here?
#     def registration(self):
#             return self._registration
#     # return model number
#     def model(self):
#             return self._model
#     # this function generating row numbers + letter
#     def seating_plan(self):
#             # 
#             return(range(1, self._num_rows + 1), "ABCSEFGHJK"[:self._num_seats_per_row])

# class AirbusA319:
#     def __init__(self, registration):
#         self._registration = registration

#     def registration(self):
#         return self._registration

#     def model(self):
#         return "Airbus A319"
    
#     def seating_plan(self):
#         return range(1, 23), "ABCDEF"

#     def num_available_seats(self):
#         # return the total of available seats by checking valuse of row
#         # return sum(sum( 1 for s in row.values() if s is None) for row in self._seating if row is not None)
#         # return sum of rows which is None 
#         return sum(sum( 1 for s in row.values() if s is None) for row in self._seating if row is not None)


# class Boeing777:
#     def __init__(self, registration):
#         self._registration = registration

#     def registration(self):
#         return self._registration

#     def model(self):
#         return "Boeing 777"
    
#     def seating_plan(self):
#         return range(1, 56), "ABCDEGHJK"

#     def num_available_seats(self):
#         # return the total of available seats by checking valuse of row
#         # return sum(sum( 1 for s in row.values() if s is None) for row in self._seating if row is not None)
#         # return sum of rows which is None 
#         return sum(sum( 1 for s in row.values() if s is None) for row in self._seating if row is not None)


# ==============changing make_glight() for polymorphism=====================================
# def make_flights():
#     # with this function we create flight object
#     f = Flight("BA758", AirbusA319("G-EUPT"))
#     # by flight object accessed to allocate_seat function to allocate seats
#     f.allocate_seat("12A", "Guido van Rossum")
#     f.allocate_seat("15F", "Bjarne stroustrup")
#     f.allocate_seat("15E", "Anders Hejlsberg")
#     f.allocate_seat("13C", "john mccarthy")
#     f.allocate_seat("14B", "Rich Hickey")

#     g = Flight("AF72", Boeing777("F-GSPS"))
#     # by flight object accessed to allocate_seat function to allocate seats
#     g.allocate_seat("55K", "Larry Wall")
#     g.allocate_seat("33G", "Yukihiro Matsumoto")
#     g.allocate_seat("4B", "Brian Kernighan")
#     g.allocate_seat("4A", "Dennis Ritchie ")
#     g.allocate_seat("14H", "Rich Hickey")
     
#     return f, g
# # create new object of the funciton to access make_boarding_cards() and print boarding pass
# f, g = make_flights()

# print(f.aircraft_model())
# print(g.aircraft_model())
# print(f.num_available_seats())
# print(g.num_available_seats())
# g.relocate_passenger("55K", "13J")

# g.make_boarding_cards(console_card_printer)

# =============================inheritance and implementation sharing====================

# lets create bas class Aircraft which contain only num_seats
# class Aircraft:
#     def __init__(self, registration):
#         self._registration = registration

#     def registration(self):
#         return self._registration

    
#     def num_seats(self):
#         # as this methods depend on _seating_plan() which does not exist in the Aircraft it gives error
#         # that we can get ride of this problem by using drive
#         rows, row_seats = self.seating_plan()
#         return len(rows) * len(row_seats)

# # drive inherit the behavior of base class
# class AirbusA319(Aircraft):

#     # moved methods with same functioanlity from both class to base class 
#     # def __init__(self, registration):
#     #     self._registration = registration

#     # def registration(self):
#     #     return self._registration

    

#     def model(self):
#         return "Airbus A319"
    
#     def seating_plan(self):
#         return range(1, 23), "ABCDEF"
#     # function count num of seats
#     # def num_seats(self):
#     #     rows, row_seats = self._seating_plan()
#     #     return len(rows) * len(row_seats)
# # drive behavior of the base class
# class Boeing777(Aircraft):
#     # moved methods wiht same functionality in base class
#     # def __init__(self, registration):
#     #     self._registration = registration

#     # def registration(self):
#     #     return self._registration

#     def model(self):
#         return "Boeing 777"
    
#     def seating_plan(self):
#         return range(1, 56), "ABCDEGHJK"

#     # function counts number of seats
#     # def num_seats(self):
#     #     rows, row_seats = self._seating_plan()
#     #     return len(rows) * len(row_seats)

# # ==============changing make_glight() for polymorphism=====================================
# # def make_flights():
# #     # with this function we create flight object
# #     f = Flight("BA758", AirbusA319("G-EUPT"))
# #     # by flight object accessed to allocate_seat function to allocate seats
# #     f.allocate_seat("12A", "Guido van Rossum")
# #     f.allocate_seat("15F", "Bjarne stroustrup")
# #     f.allocate_seat("15E", "Anders Hejlsberg")
# #     f.allocate_seat("13C", "john mccarthy")
# #     f.allocate_seat("14B", "Rich Hickey")

# #     g = Flight("AF72", Boeing777("F-GSPS"))
# #     # by flight object accessed to allocate_seat function to allocate seats
# #     g.allocate_seat("55K", "Larry Wall")
# #     g.allocate_seat("33G", "Yukihiro Matsumoto")
# #     g.allocate_seat("4B", "Brian Kernighan")
# #     g.allocate_seat("4A", "Dennis Ritchie ")
# #     g.allocate_seat("14H", "Rich Hickey")
     
# #     return f, g
# # create new object of the funciton to access make_boarding_cards() and print boarding pass

# # print(f.num_available_seats())
# # print(g.num_available_seats())

# a = AirbusA319("G-EZBT")
# print(a.num_seats())
# b = Boeing777("N717AN")
# print(b.num_seats())

# =============================File IO and Resource Managements================================
  

# f = open('wasteland.txt', mode='wt', encoding='utf-8')
# all text file should contain one on read, write, and append mode
# one of preceding should combine one of the selector b for binary mode and t for text mode
# 'wb' open for writning in binary
# 'at' append text
# object can return a file like an object
# 'rt' read text
# appending text
# h = open('/home/sigma/pluralsight_project/python042022/wasteland.txt', mode='at', encoding='utf-8')

# h.writelines(
#     ['son of man, \n',
#      'you cannot say, or guess,',
#       'for you know only, \n',
#        'a heap of broken images,',
#         'where the sun beats\n'])
# h.close()

# =======================iterating over files============================
# file object support iterator protoclol
# each iteration create new line in file
# import sys

# f = open('/home/sigma/pluralsight_project/python042022/wasteland.txt', mode='rt', encoding='utf-8')

# for line in f:
#     print(line)
# f.close()
# # now we can see a lot of free spaces while reading the file to get ride of that we 
# # use another standard library

# import sys

# f = open('/home/sigma/pluralsight_project/python042022/wasteland.txt', mode='rt', encoding='utf-8')

# for line in f:
#     print("using simple print\n",line)
#     print("below print is using stdout", sys.stdout.write(line))
    
# f.close()

# =====================the recaman sequence========================
# import sys
# from itertools import count, islice
# # need a function to generate sequence number with one number per line 
# # we will see the way of generating numeric data
# # sequence generator
# def sequence():
#     # creat an empty set
#     seen = set()
#     a = 0
#     # count(1) built-in function = means count the number of time an object appears in a list
#     # the number of times a given values occures in a string or a list
#     # count numbers in a given set of arrays.
#     # each time generate one number per line
#     # count(1) means stpes is = 1

#     for n in count(1):
#         yield a 
#         seen.add(a)
#         c = a - n
#         if c<0 or c in seen:
#             c = a + n
#         a = c

# def write_sequence(filename, num):
#     # write recaman's sequence to a text file
#     # f = open('/home/sigma/pluralsight_project/python042022/Recaman_Sequence.txt', mode='wt', enconding='utf-8')
#     # islice is used to truncate 
#     f = open(filename, mode='wt')
#     f.writelines(f"{r}\n" 
#                   for r in islice(sequence(), num + 1))
#     f.close()
    
# # if this is the main madule
# if __name__ == '__main__':
#     # excute funciton write_sequence() 
#     # first argument variable open file,
#     # second argument variable wrtie int nummber
#     write_sequence(filename=sys.argv[1], 
#                     num=int(sys.argv[2]))

# ==========sequence reader===========================
# import sys
# # read the generated recaman sequences from this path
# filename = '/home/sigma/pluralsight_project/python042022/recaman.txt'
# # print(filename)
# # now i pass the path as function parameter 
# def read_series(filename):

#     try:

#     # read the file that passed as function parameter
#         f = open(filename, mode='rt', encoding='utf-8')
#         return[int(line.strip()) for line in f]
#         # create empty list
#         series = []
#         # now read line by line and remove the space between numbers using strip()
#         # for line in f:
#         #     a = int(line.strip())
#         #     # append to the list each time
#         #     series.append(a)
#     finally:
#         f.close()
#         # return the final list
#         # return series
#     # call the function and print the values
#     # s = read_series(filename)
#     # print(s)

# def main(filename):
#     series = read_series(filename)
#     print(sorted(series))
# main(filename)

# main(filename)
# now run from command line
# =============closing files with finally=====================================
# as in the above code f.close() is not excuting we put it in finally inordert to close file
# it give another opportunity to put the return lines in a list



#  ================ File Usage Pattern=========================
# modify previous code

# import sys
# # read the generated recaman sequences from this path
# filename = '/home/sigma/pluralsight_project/python042022/recaman.txt'
# # print(filename)
# # now i pass the path as function parameter 
# def read_series(filename):

#     # read the file that passed as function parameter
#         with open(filename, mode='rt', encoding='utf-8') as f:
#             return[int(line.strip()) for line in f]

# def main(filename):
#     series = read_series(filename)
#     print(sorted(series))
# main(filename)

# ================modify write_sequence()=======================================

# filename = '/home/sigma/pluralsight_project/python042022/recaman.txt'

# import sys
# from itertools import count, islice
# # need a function to generate sequence number with one number per line 
# # we will see the way of generating numeric data
# # sequence generator
# def sequence():
#     # creat an empty set
#     seen = set()
#     a = 0
#     # count(1) built-in function = means count the number of time an object appears in a list
#     # the number of times a given values occures in a string or a list
#     # count numbers in a given set of arrays.
#     # each time generate one number per line
#     # count(1) means stpes is = 1

#     for n in count(1):
#         yield a 
#         seen.add(a)
#         c = a - n
#         if c<0 or c in seen:
#             c = a + n
#         a = c

# # def write_sequence(filename, num):
# #     # write recaman's sequence to a text file
# #     # f = open('/home/sigma/pluralsight_project/python042022/Recaman_Sequence.txt', mode='wt', enconding='utf-8')
# #     # islice is used to truncate 
# #     f = open(filename, mode='wt')
# #     f.writelines(f"{r}\n" 
# #                   for r in islice(sequence(), num + 1))
# #     f.close()

# def write_sequence(filename, num):
#     with open(filename, mode='wt', encoding='utf-8') as f:
#         f.writelines(f"{r}\n"
#                      for r in islice(sequence(), num + 1))

# # if this is the main madule
# if __name__ == '__main__':
#     # excute funciton write_sequence() 
#     # first argument variable open file,
#     # second argument variable wrtie int nummber
#     write_sequence(filename=sys.argv[1], 
#                     num=int(sys.argv[2]))

# ===================================== Binary Files===============================
# int. from_bytes()

# def write_grayscale(filename, pixels):
#     """create and writes a grayscale BMP file.
#     Args:
#         filename: the name of the BMP file to me created.
        
#         pixels: a rectagular image stored as a sequence of rows.
#             each row must be an iterable series of integers in the 
#             range 0-255.
    
#     Raises:
#         valueerror: if any of the integer values are out of range.
#         OSError: if the file could not be written.
    
#     """
#     height = len(pixels)
#     width = len(pixels[0])

#     with open(filename, 'wb') as bmp:
#         # BMP Header
#         bmp.write(b'BM')

#         # the next four bytes hold the filesize as a 32-bit
#         size_bookmark = bmp.tell()
#         # little-endian integer. zero placeholder for now.
#         bmp.write(b'\x00\x00\x00\x00')

#         # unused 16-bit integer - should be zero
#         bmp.write(b'\x00\x00')
#         # unused 16-bit integer should be zero
#         bmp.write(b'\x00\x00')

#         # the next four bytes hold the integer offset to the 
#         pixel_offset_bookmark = bmp.tell()
#         # pixel data. zero placeholder for now.
#         bmp.write(b'\x00\x00\x00\x00')

#         # image header
#         # image header size in bytes - 40 decimal
#         bmp.write(b'\x28\x00\x00\x00')
#         # image width in pixels
#         bmp.write(_int32_to_bytes(width))
#         # image hight in pixels
#         bmp.write(_int32_to_bytes(height))
#         # number of images planes
#         bmp.write(b'\x01\x00')
#         # bits per pixel 8 for grayscale
#         bmp.write(b'\x08\x00')
#         # no compression
#         bmp.write(b'\x00\x00\x00\x00')
#         # zero for uncompressed image
#         bmp.write(b'\x00\x00\x00\x00')
#         # unused pixels per meter
#         bmp.write(b'\x00\x00\x00\x00')
#         # unused pixels per meter
#         bmp.write(b'\x00\x00\x00\x00')
#         # use whole color table
#         bmp.write(b'\x00\x00\x00\x00')
#         # all colors are important
#         bmp.write(b'\x00\x00\x00\x00')

#         # color palette - a linear grayscale
#         for c in range(256):
#             # blue, green, red, zero
#              bmp.write(bytes((c, c, c, 0)))
        
#         # pixel data
#         pixel_data_bookmark = bmp.tell()
#         # bmp files are bottom to top
#         for row in reversed(pixels):
#             row_data = bytes(row)
#             bmp.write(row_data)
#             # pad row to multiple of four bytes
#             padding = b'\x00' * ((4 - (len(row) % 4)) % 4)
#             bmp.write(padding)

#         # end of file
#         eof_bookmark = bmp.tell()

#         # fill in file size placeholder
#         bmp.seek(size_bookmark)
#         bmp.write(_int32_to_bytes(eof_bookmark))

#         # fill in pixel offset placehol
#         bmp.seek(pixel_offset_bookmark)
#         bmp.write(_int32_to_bytes(pixel_data_bookmark))

# def _int32_to_bytes(i):
#     """convert an integer to four bytes in little-endian format."""
#     # &: bitwise-and
#     # >>: right-shift

#     return bytes((i & 0xff,
#                     i >> 8 & 0xff,
#                     i >> 16 & 0xff,
#                     i >> 24 & 0xff))

# def dimensions(filename):
#     """determine the dimensions in pixels of a bmp image.
#     Args:
#         filename: the filename of a bmp file.
#     Returns:
#         a tuple containing two integer with the width
#         and height in pixels.

#     Raises:
#         valueerror: if the file was not a bmp file.
#         oserror: if there was a problem reading the file.
#     """

#     with open(filename, 'rb') as f:
#         magic = f.read(2)
#         if magic !=b'BM':
#             raise ValueError("{} is not a BMP file". format(filename))

#         f.seek(18)
#         width_bytes = f.read(4)
#         height_bytes = f.read(4)

#         return (_bytes_to_int32(width_bytes),
#                 _bytes_to_int32(height_bytes))

# def _bytes_to_int32(b):
#     return b[0] | (b[1] << 8) | (b[2] << 16 | (b[3] << 24))


# import math

# def mandel(real, imag):
#     """the logarith of number of iterations nee"""
#     x = 0
#     y = 0
#     for i in range(1, 257):
#         if x*x + y*y > 4.0:
#             break
#         xt = real + x*x - y*y 
#         y = imag + 2.0 *x * y 
#         x = xt

#     return int(math.log(i) * 256 / math.log(256)) - 1

# def mandelbrot(size_x, size_y):

#     return [[mandel((3.5 * x / size_x) - 2.5,
#                     (2.0 * y / size_y) - 1.0)
#             for x in range(size_x)]
#             for y in range(size_y)]

# # ==============problem================================

# import fractal
# pixels = fractal.mandelbrot(448, 256)
# import reprlib
# # write those pixels to a bmp file
# reprlib.repr(pixels)
# import bmp
# bmp.write_grayscale("mandel.bmp", pixels)
# # then it present "the Mandelbrot Set"

# =====================File-like objects======================================

# def words_per_line(flo):
#      return [len(line.split()) for line in flo.readlines()]

# with open("/home/sigma/pluralsight_project/python042022/wasteland.txt", mode='rt', encoding='utf-8') as real_file:
#     wpl = words_per_line(real_file)

# print(wpl)

# print(type(real_file))

# # object can be any type of file

# from urllib.request import urlopen
# with urlopen("http://sixty-north.com/c/t.txt") as web_file:
#     wfpl = words_per_line(web_file)
# print(wfpl)
# print(type(web_file))
# # the second type of object is HTTP
# means an object can be any type of file it does not make any difference

# ===================context managers============================

# class which is usable with context manager protocols.

# class RefrigeratorRaider:
#     """Raid a refrigerator"""

#     def open(self):
#         print("open fridge door.")

#     def take(self, food):
#         print(f"finding {food}")
#         if food == 'deep fried pizza':
#             raise RuntimeError("health warning!")
#         print(f"Taking {food}")

#     def close(self):
#         print("close fridge door.")

# def raid(food):
#     r = RefrigeratorRaider()
#     r.open()
#     r.take(food)
#     r.close()
# raid('bacon')
# # after taking bacon it will close the door
# # but when we take "deep fried pizza" we did not get around closing the door
# # to handle this problem 

# from contextlib import closing

# class RefrigeratorRaider:
#     """Raid a refrigerator"""

#     def open(self):
#         print("open fridge door.")

#     def take(self, food):
#         print(f"finding {food}")
#         if food == 'deep fried pizza':
#             raise RuntimeError("health warning!")
#         print(f"Taking {food}")

#     def close(self):
#         print("close fridge door.")

# def raid(food):
#     with closing(RefrigeratorRaider()) as r:
#     # r = RefrigeratorRaider()
#         r.open()
#         r.take(food)
#         # r.close()
# # raid('bacon')
# raid('deep fried pizza')
# we see the close() is called twice
# the explicit call of close() is unnecessary lets remove it or comment it in above 
# now we can see even there is runtimeerror but still the door closed

with open('/home/sigma/pluralsight_project/python042022/wasteland.txt', 'w') as opened_file:
    opened_file.write('Hola!')

"""the above code pens the file and write some data to it and then close it. if any error
    if any error occurs while writing the data to the file, it tries to close it.
    the above code is equivalent to:

"""

file = open('some_file', 'w')
try:
    file.write('Hola!')
finally:
    file.close()

"""the main advantage of with is make sure the file is closed and make 
and make the code shorter. a common use case of context manager is locking and nlocking
resources and closing opened files """ 





















































 